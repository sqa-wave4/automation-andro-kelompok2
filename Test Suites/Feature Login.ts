<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>All Feature login</description>
   <name>Feature Login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>06326b8e-8338-49e1-a636-7231bd104842</testSuiteGuid>
   <testCaseLink>
      <guid>890bf22a-b7ce-4bf6-98b3-9c81e4a9f50f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Step Definition/Feature Login/LGI002 - User login using registered email and incorrect password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>c31c6d57-83b2-434a-96c6-c1756fbcb194</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Step Definition/Feature Login/LGI003 - User login using unregistered email</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>920c229c-5402-4a70-b42e-e8b9b955da8d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Step Definition/Feature Login/LGI001 - User login using registered email and correct password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
