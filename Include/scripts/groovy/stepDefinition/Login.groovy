package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import internal.GlobalVariable

public class Login {

	@Given("User already on login page")
	public void user_already_on_login_page() {
		Mobile.callTestCase(findTestCase('Pages/Page Akun/Tap Button Masuk'), [:], FailureHandling.STOP_ON_FAILURE)
		Mobile.callTestCase(findTestCase('Pages/Page Masuk/Verify Element Masuk'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User input registered email")
	public void user_input_registered_email() {
		Mobile.callTestCase(findTestCase('Pages/Page Masuk/Input Email - Login'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User input correct password")
	public void user_input_correct_password() {
		Mobile.callTestCase(findTestCase('Pages/Page Masuk/Input Password - Login'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User tap button masuk")
	public void user_tap_button_masuk() {
		Mobile.callTestCase(findTestCase('Pages/Page Masuk/Tap Button Masuk'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User input unregistered email")
	public void user_input_unregistered_email() {
		Mobile.callTestCase(findTestCase('Pages/Page Masuk/Input Random Email - Login'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User input random password")
	public void user_input_random_password() {
		Mobile.callTestCase(findTestCase('Pages/Page Masuk/Input Random Password - Login'), [:], FailureHandling.CONTINUE_ON_FAILURE)
	}

	@Then("User successfully login")
	public void user_successfully_login() {
		Mobile.callTestCase(findTestCase('Pages/Page Akun/Verify Element Akun - after login'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User failed login")
	public void user_failed_login() {
		Mobile.callTestCase(findTestCase('Pages/Page Masuk/Verify Element Masuk'), [:], FailureHandling.STOP_ON_FAILURE)
		Mobile.callTestCase(findTestCase('Pages/Page Akun/Verify Element Akun - failed login'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}
