package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import internal.GlobalVariable

public class Logout {
	@Given("User on Akun page")
	public void user_on_Akun_page() {
		Mobile.callTestCase(findTestCase('Pages/Page Beranda/Tap Navigation Bar Akun'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User tap button logout")
	public void user_tap_button_logout() {
		Mobile.callTestCase(findTestCase('Pages/Page Akun/Tap Button Keluar'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user logged out")
	public void user_logged_out() {
		Mobile.callTestCase(findTestCase('Pages/Page Beranda/Verify Element Beranda'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}
