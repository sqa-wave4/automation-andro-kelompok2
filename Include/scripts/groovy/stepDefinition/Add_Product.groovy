package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import internal.GlobalVariable

public class Add_Product {
	@Given("User already on tambah produk page")
	public void user_already_tambah_produk_page() {
		Mobile.callTestCase(findTestCase('Pages/Page Beranda/Tap Icon Add Product'), [:], FailureHandling.STOP_ON_FAILURE)
		Mobile.callTestCase(findTestCase('Pages/Page Tambah Produk/Verify Element Tambah Produk'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User input nama produk")
	public void user_input_nama_produk() {
		Mobile.callTestCase(findTestCase('Pages/Page Tambah Produk/Input Nama Produk'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User input harga produk")
	public void user_input_harga_produk() {
		Mobile.callTestCase(findTestCase('Pages/Page Tambah Produk/Input Harga Produk'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User choose category produk")
	public void user_choose_category_produk() {
		Mobile.callTestCase(findTestCase('Pages/Page Tambah Produk/Choose Kategori Produk'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User input lokasi produk")
	public void user_input_lokasi_produk() {
		Mobile.callTestCase(findTestCase('Pages/Page Tambah Produk/Input Lokasi Produk'), [:], FailureHandling.STOP_ON_FAILURE)
		Mobile.callTestCase(findTestCase('Pages/Page Beranda/Swipe Down'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User input deskripsi produk")
	public void user_input_deskripsi_produk() {
		Mobile.callTestCase(findTestCase('Pages/Page Tambah Produk/Input Deskripsi'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User choose foto produk")
	public void user_choose_foto_produk() {
		Mobile.callTestCase(findTestCase('Pages/Page Tambah Produk/Choose Photo'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User click button terbitkan")
	public void user_click_button_terbitkan() {
		Mobile.callTestCase(findTestCase('Pages/Page Tambah Produk/Tap Button Terbitkan'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User click button preview")
	public void user_click_button_preview() {
		Mobile.callTestCase(findTestCase('Pages/Page Tambah Produk/Tap Button Preview'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User success add product")
	public void user_success_add_product() {
		Mobile.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Verify Element Icon Add Product'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}
