package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Search {
	@Given("User access Beranda page")
	public void User_access_Beranda_page() {
		Mobile.callTestCase(findTestCase('Pages/Page Search/Access Beranda page'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User tap Semua button")
	public void User_tap_Semua_button() {
		Mobile.callTestCase(findTestCase('Pages/Page Search/Tap button search by Semua - Search by category'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Product list with Semua category displayed")
	public void Product_list_with_Semua_category_displayed() {
		Mobile.callTestCase(findTestCase('Pages/Page Search/Wait for product list displayed - Search by category'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User tap Elektronik button")
	public void User_tap_Elektronik_button() {
		Mobile.callTestCase(findTestCase('Pages/Page Search/Tap button search by Elektronik - Search by category'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Product list with Elektronik category displayed")
	public void Product_list_with_Elektronik_category_displayed() {
		Mobile.callTestCase(findTestCase('Pages/Page Search/Wait for product list displayed - Search by category'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User tap KomputerAksesoris button")
	public void User_tap_KomputerAksesoris_button() {
		Mobile.callTestCase(findTestCase('Pages/Page Search/Tap button search by KomputerAksesoris - Search by category'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Product list with KomputerAksesoris category displayed")
	public void Product_list_with_KomputerAksesoris_category_displayed() {
		Mobile.callTestCase(findTestCase('Pages/Page Search/Wait for product list displayed - Search by name'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User tap Search field on Beranda page")
	public void User_tap_Search_field_on_Beranda_page() {
		Mobile.callTestCase(findTestCase('Pages/Page Search/Tap search field - Serach by name'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User input name of product")
	public void User_input_name_of_product() {
		Mobile.callTestCase(findTestCase('Pages/Page Search/Input product name - Search by name'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Product list displayed")
	public void Product_list_displayed() {
		Mobile.callTestCase(findTestCase('Pages/Page Search/Wait for product list displayed - Search by name'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}