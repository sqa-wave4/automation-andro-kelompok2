package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

public class BerhasilTerjual {
	
	@Given("User is going to my sell list page")
	public void user_is_going_to_my_sell_list_page() {
		Mobile.callTestCase(findTestCase('Pages/Page Beranda/Tap Navigation Bar Akun'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User stays at Daftar Jual Saya page")
	public void user_stays_at_Daftar_Jual_Saya_page() {
		Mobile.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Navigate to Daftar Jual Saya Page'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User come to the Diminati page")
	public void user_come_to_the_Diminati_page() {
		Mobile.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Navigate to Diminati Page'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User want to see Detail Product page")
	public void user_want_to_see_Detail_Product_page() {
		Mobile.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Page Diminati/Go to the Detail Produk - Berhasil Terjual'),
	[:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User tap button Terima")
	public void user_tap_button_Terima() {
		Mobile.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Page Diminati/Tap button Terima'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User swipe popup wa")
	public void user_swipe_popup_wa() {
		Mobile.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Page Diminati/Drag Popup Hubungi WA'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User clicks button Status")
	public void user_clicks_button_Status() {
		Mobile.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Page Diminati/Tap button Status'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User tap button Berhasil Terjual")
	public void user_tap_button_Berhasil_Terjual() {
		Mobile.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Page Diminati/Tap button Berhasil terjual'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User confirmation with click Simpan button")
	public void user_confirmation_with_click_Simpan_button() {
		Mobile.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Page Diminati/Tap button Simpan'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}
