package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

public class Terjual {
	
	@Given("User go to the Daftar Jual Saya page from button akun")
	public void user_go_to_the_Daftar_Jual_Saya_page_from_button_akun() {
		Mobile.callTestCase(findTestCase('Pages/Page Beranda/Tap Navigation Bar Akun'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("User on the Daftar Jual Saya page")
	public void user_on_the_Daftar_Jual_Saya_page() {
		Mobile.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Navigate to Daftar Jual Saya Page'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("User already on Terjual page")
	public void user_already_on_Terjual_page() {
		Mobile.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Navigate to Terjual Page'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@Then("User in the detail product")
	public void user_in_the_detail_product() {
		Mobile.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Page Terjual/Go to the Detail Produk Terjual'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}
