package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Negotiation {

	@Given("User login the app")
	public void User_login_the_app() {
		Mobile.callTestCase(findTestCase('Step Definition/Feature Login/LGI001 - User login using registered email and correct password'),
				[:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User access Beranda page")
	public void User_access_Beranda_page() {
		Mobile.callTestCase(findTestCase('Pages/Page Search/Tap search field - Serach by name'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User tap the product")
	public void User_tap_the_product() {
		Mobile.callTestCase(findTestCase('Pages/Page Negotiation/Tap Product'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User tap Saya Tertarik dan Ingin Nego")
	public void User_tap_Saya_Tertarik_dan_Ingin_Nego() {
		Mobile.callTestCase(findTestCase('Pages/Page Negotiation/Tap button SayaTertarikNego'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User do not input the price")
	public void User_do_not_input_the_price() {
		Mobile.callTestCase(findTestCase('Pages/Page Negotiation/Input without price'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User tap Kirim button")
	public void User_tap_Kirim_button() {
		Mobile.callTestCase(findTestCase('Pages/Page Negotiation/Tap button Kirim'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Error message displayed")
	public void Error_message_displayed() {
		Mobile.callTestCase(findTestCase('Pages/Page Negotiation/Verify element error message'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User input the price")
	public void User_input_the_price() {
		Mobile.callTestCase(findTestCase('Pages/Page Negotiation/Input with price'), [:], FailureHandling.STOP_ON_FAILURE)
	}


	@Then("Success message displayed")
	public void Success_message_displayed() {
		Mobile.callTestCase(findTestCase('Pages/Page Negotiation/Verify element success message'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}