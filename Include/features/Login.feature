@Login
Feature: Login

  @LGI002
  Scenario: User want to login using registered email and incorrect password
    Given User already on login page
    When User input registered email
    And User input random password
    And User tap button masuk
    Then User failed login

  @LGI003
  Scenario: User want to login using unregistered email
    Given User already on login page
    When User input unregistered email
    And User input random password
    And User tap button masuk
    Then User failed login

  @LGI001
  Scenario: User want to login using registered email and correct password
    Given User already on login page
    When User input registered email
    And User input correct password
    And User tap button masuk
    Then User successfully login
