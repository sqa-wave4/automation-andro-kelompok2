@Search
Feature: Search
  User want to search product

  @SCR001
  Scenario: User want to search product by Semua category 
    Given User access Beranda page
    When User tap Semua button
    Then Product list with Semua category displayed
    
  @SCR002
  Scenario: User want to search product by Elektronik category 
    Given User access Beranda page
    When User tap Elektronik button
    Then Product list with Elektronik category displayed
    
  @SCR003
  Scenario: User want to search product by KomputerAksesoris category 
    Given User access Beranda page
    When User tap KomputerAksesoris button
    Then Product list with KomputerAksesoris category displayed
    
  @SCR004
  Scenario: User want to search product by product name
    Given User access Beranda page
    When User tap Search field on Beranda page
    And User input name of product
    Then Product list displayed
