@Update
Feature: Logout

  @PRO001
  Scenario: User want to update nama and new password
    Given User on dashboard
    When User tap button profile
    And User tap button edit
    And User tap textview nama
    And User update nama
    And User tap textview password
    And User update new password
    Then User success update the profile