@batalkan
Feature: batalkan

  Scenario: User Accept the Offer from Buyer and Change the Status to Batalkan Transaksi
    Given User wants to go Daftar Jual Saya page from Akun
    When User is in Daftar Jual Saya page
    And User stay in Diminati page
    And User come to Detail Product page
    Then User click button Terima
    And User drag popup hubungi wa
    And User tap button Status
    And User click button Batalkan Transaksi
    And User tap Simpan button
