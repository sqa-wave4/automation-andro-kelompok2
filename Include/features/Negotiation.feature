@Negotiation
Feature: Negotiation
  User want to negotioning the product

  @NEG001
  Scenario: User want to negotioning product without input the price
    Given User login the app
    When User access Beranda page
    And User tap the product
    And User tap Saya Tertarik dan Ingin Nego
    And User do not input the price
    And User tap Kirim button
    Then Error message displayed

  @NEG002
  Scenario: User want to negotioning product with input price
    Given User login the app
    When User access Beranda page
    And User tap the product
    And User tap Saya Tertarik dan Ingin Nego
    And User input the price
    And User tap Kirim button
    Then Success message displayed