import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.verifyElementVisible(findTestObject('Page_Profile/label_lengkapi_info_akun_2'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Profile/btn_add_photo_2'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Profile/label_info_profil_2'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Profile/label_nama_2'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Profile/viewGroup_nama_2'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Profile/label_no_hp_2'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Profile/viewGroup_no_hp_2'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Profile/label_kota_2'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Profile/viewGroup_kota_2'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Profile/label_alamat_2'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Profile/viewGroup_alamat_2'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Profile/label_info_akun_2'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Profile/label_email_2'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Profile/viewGroup_email_2'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Profile/label_pwd_2'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Profile/viewGroup_pwd_2'), 0)

